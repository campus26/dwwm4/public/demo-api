<?php

namespace App\Controller;

use OpenAI;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Routing\Attribute\Route;

class ApiController extends AbstractController
{
    #[Route('/prompt/{nbStep}/{start}/{end}/{category}', name: 'app_prompt', methods:["GET"] )]
    public function prompt($nbStep, $start, $end, $category): Response
    {
        $yourApiKey = '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';
        $client = OpenAI::client($yourApiKey);

        // $nbStep = 4;
        // $start = "Paris";
        // $end = "Marseille";
        $prompt = "Peux tu me donner en ".$nbStep." étapes entre ".$start." et ".$end." sur 4 jours un itinéraire touristique ".$category. ", avec les activités possibles pour chaque étape, avec la durée et la géocalisation des activités.
            Pour chaque étape il me faudra obligatoirement la latitude et la longitude.
            Les données à l'intérieur de la réponse devront être en français.
            La structure de données devra respecter l'interface suivante :
            [jour:{latidude:'',longitude:'',duree:'',activite:[]}]
            La réponse devra obligatoirement être au format JSON sans aucun commentaire.";
        $result = $client->chat()->create([
            'model' => 'gpt-3.5-turbo',
            'messages' => [
                ['role' => 'user', 'content' => $prompt],
            ],
        ]);

        return new Response($result->choices[0]->message->content); // Hello! How can I assist you today?

    }

    #[Route('/ors', name: 'app_ors', methods: ["GET"])]
    public function ors(): JsonResponse
    {
        $api_key = "5b3ce3597851110001cf62486d469454b4084a62a7583c467d04121c";
        $client = new \GuzzleHttp\Client();
        $response = $client->request('GET', 'https://api.openrouteservice.org/v2/directions/driving-car?api_key=' . $api_key . '&start=8.681495,49.41461&end=8.687872,49.420318');

        $data = json_decode($response->getBody()); // '{"id": 1420053, "name": "guzzle", ...}'
        $retour = $data->features[0]->geometry;
        return new JsonResponse($retour);
    }

    #[Route('/hello', name: 'app_hello', methods: ["GET"])]
    public function hello(KernelInterface $appKernel): Response
    {
        dump($appKernel->getProjectDir());
        return new Response("HELLO");
    }
}
